import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceApiService } from './services/service-api.service';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  wordResponse: object[];
  constructor(private serviceApi: ServiceApiService) {
    this.serviceApi.fechDataApi();
    this.wordResponse = this.serviceApi.getDataApi();
    console.log(this.wordResponse);
  }

}
