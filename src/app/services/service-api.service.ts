import { Injectable } from '@angular/core';
import { response } from '../api/mockApi';


@Injectable({
  providedIn: 'root'
})

export class ServiceApiService {
  apiResponse: object[];
  constructor() { }

  public fechDataApi() {
    this.apiResponse = response;
  }
  public getDataApi(): object[]{
    return this.apiResponse;
  }
}
